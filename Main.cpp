/*

  Main.cpp

*/

#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include "Parser.hpp"





std::vector<pid_t> Children;



void KillChildren(int Signal){

	if( Children.empty() )
		return;

	pid_t ChildrenPgid = Children[0];

	if( kill(-ChildrenPgid, Signal) == -1 )
		std::cerr << "Failed to kill children whose PGID is " << ChildrenPgid << '.';

	std::cerr << std::endl;

	}



void PrintPrompt(void) {

	std::cerr << "baash-" __REVISION__ "$ ";
	std::cerr.flush();

	}



int main() {

	sigset_t SignalSet;
	pid_t ShellPgid = getpid();

	// Refuse to operate on non-tty.
	if( isatty(STDIN_FILENO) == 0 || isatty(STDOUT_FILENO) == 0 || isatty(STDERR_FILENO) == 0 ){

		std::cerr << "IO is not performed on TTY." << std::endl;
		return -1;

		}

	if( signal(SIGINT, KillChildren) == SIG_ERR ||
	    signal(SIGQUIT, KillChildren) == SIG_ERR ||
	    signal(SIGTTIN, SIG_IGN) == SIG_ERR ||
	    signal(SIGTTOU, SIG_IGN) == SIG_ERR ||
	    signal(SIGTSTP, SIG_IGN) == SIG_ERR ||
	    signal(SIGCHLD, SIG_IGN) == SIG_ERR ){

		std::cerr << "Faile to set signal handler." << std::endl;
		return -1;

		}

	if( setpgid(0, 0) == -1 ){

		std::cerr << "Failed to set BAASH to group leader." << std::endl;
		return -1;

		}

	if( sigemptyset(&SignalSet) == -1 ||
	    sigaddset(&SignalSet, SIGINT) == -1 ||
	    sigaddset(&SignalSet, SIGQUIT) == -1 ){

		std::cerr << "Failed to set signal set." << std::endl;
		return -1;

		}

	std::string CommandBuffer;

	std::cerr << "Baash Ain't A SHell revision " __REVISION__ " built on " __DATE__ << std::endl;

	PrintPrompt();

	while( std::getline(std::cin, CommandBuffer) ) {

		std::vector<command_t> CommandQueue;

		// Parse.
		try{

			CommandBuffer += ' ';
			CommandQueue = Parse( Tokenize(CommandBuffer) );

			}
		// My exceptions.
		catch(const char* ErrorMessage){

			std::cerr << ErrorMessage << std::endl;
			CommandQueue.clear();

			}
		// C++ exceptions.
		catch(const std::exception& StandardException){

			std::cerr << StandardException.what() << std::endl;
			CommandQueue.clear();

			}
		// Unkown exception.
		catch(...){

			std::cerr << "BAASH can't behave herself. Please report her." << std::endl;
			CommandQueue.clear();

			}

		int PrevPipe = -1;

		if( sigprocmask(SIG_BLOCK, &SignalSet, NULL) == -1 ){

			std::cerr << "Failed to change signal mask." << std::endl;
			break;

			}

		// Executing command.
		for(auto It = CommandQueue.begin(); It != CommandQueue.end(); ++It){

			It->Argument.push_back( nullptr );

			bool CreatedPipe = false;
			int PipeFd[2] = {-1, -1};

			if( (It + 1) != CommandQueue.end() ){

				if( pipe(PipeFd) < 0 ){

					std::cerr << "Failed to create pipe." << std::endl;

					if( PrevPipe != -1 )
						close(PrevPipe);

					break;

					}

				CreatedPipe = true;

				}

			pid_t Pid = fork();

			if( Pid < 0 ){

				std::cerr << "Failed to fork." << std::endl;

				if( PrevPipe != -1 )
					close(PrevPipe);

				if( CreatedPipe ){

					close(PipeFd[0]);
					close(PipeFd[1]);

					}

				}
			// Child.
			else if( Pid == 0 ){

				if( PrevPipe != -1 ){

					dup2(PrevPipe, STDIN_FILENO);
					close(PrevPipe);

					}

				if( CreatedPipe ){

					dup2(PipeFd[1], STDOUT_FILENO);
					close(PipeFd[1]);

					}

				// This will overwrite pipe.
				if( It->Stdin != nullptr ){

					int Fd = open(It->Stdin, O_RDONLY);

					if( Fd == -1 ){

						std::cerr << "Redirect source \'" << (It->Stdin) << "\' doesn't exist." << std::endl;
						return -1;

						}

					dup2(Fd, STDIN_FILENO);
					close(Fd);

					}

				// This will overwrite pipe.
				if( It->Stdout != nullptr ){

					int Fd = open(It->Stdout, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

					if( Fd == -1 ){

						std::cerr << "Failed to create redirect target \'" << (It->Stdout) << "\'." << std::endl;
						return -1;

						}

					dup2(Fd, STDOUT_FILENO);
					close(Fd);

					}

				if( sigprocmask(SIG_UNBLOCK, &SignalSet, NULL) == -1 )
					std::cerr << "Failed to change signal mask." << std::endl;

				pid_t ChildPgid = Children.empty() ? 0 : Children[0];

				if( setpgid(0, ChildPgid) == -1 )
					std::cerr << "Failed to set PGID." << std::endl;

				execvp(It->Argument[0], It->Argument.data());

				std::cerr << "Failed to exec \'" << (It->Argument[0]) << "\'." << std::endl;
				return -1;

				}
			else
				Children.push_back( Pid );

			if( PipeFd[1] != -1 )
				close(PipeFd[1]);

			if( PrevPipe != -1 )
				close(PrevPipe);

			PrevPipe = PipeFd[0];

			}

		CommandQueue.clear();

		if( sigprocmask(SIG_UNBLOCK, &SignalSet, NULL) == -1 )
			std::cerr << "Failed to change signal mask." << std::endl;

		if( tcsetpgrp(STDIN_FILENO, Children[0]) == -1 ||
		    tcsetpgrp(STDOUT_FILENO, Children[0]) == -1 ||
		    tcsetpgrp(STDERR_FILENO, Children[0]) == -1 )
			std::cerr << "Failed to set foreground process" << std::endl;

		for(pid_t Child: Children){

			int ReturnedValue = 0;
			waitpid(Child, &ReturnedValue, 0);

			}

		if( tcsetpgrp(STDIN_FILENO, ShellPgid) == -1 ||
		    tcsetpgrp(STDOUT_FILENO, ShellPgid) == -1 ||
		    tcsetpgrp(STDERR_FILENO, ShellPgid) == -1 )
			std::cerr << "Failed to set foreground process" << std::endl;

		Children.clear();
		PrintPrompt();

		}

	std::cerr << std::endl;
	return 0;

	}
