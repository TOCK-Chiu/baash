/*

  Parser.hpp

*/





#ifndef __PARSER__HPP__
#define __PARSER__HPP__

#if !defined(__cplusplus) || (__cplusplus < 201103L)
#error "C++11 support is required."
#endif

#include <cstdlib>
#include <string>
#include <vector>





struct command_t {

	std::vector<char*> Argument;
	char* Stdin;
	char* Stdout;

	command_t():
		Stdin( nullptr ),
		Stdout( nullptr ){
		;
		}

	command_t(const command_t& ) = delete;

	command_t(command_t&& RHS):
		Argument( std::move(RHS.Argument) ),
		Stdin( RHS.Stdin ),
		Stdout( RHS.Stdout ){

		RHS.Argument.clear();
		RHS.Stdin = RHS.Stdout = nullptr;

		}

	command_t& operator=(const command_t& ) = delete;
	command_t& operator=(command_t&& ) = delete;

	~command_t(){

		free(Stdin);
		free(Stdout);

		for(char* Ptr: Argument)
			free(Ptr);

		}

	};



// Convert String into a bunch of token string.
std::vector<std::string> Tokenize(const std::string& String);

// Convert a bunch of token string into a command queue;
std::vector<command_t> Parse(const std::vector<std::string>& TokenPile);





#endif
