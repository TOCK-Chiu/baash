/*

  Parser.cpp

*/





#include <cctype>
#include <cstring>
#include <utility>
#include "Parser.hpp"





std::vector<std::string> Tokenize(const std::string& String) {

	std::string Buffer;
	std::vector<std::string> TokenPile;
	enum { Ready, Quote, Str } Status = Ready;

	for(char Char: String){

		if( Status == Quote ){

			if( Char == '\'' ){

				TokenPile.emplace_back( std::move(Buffer) );
				Buffer.clear();
				Status = Ready;

				}
			else
				Buffer += Char;

			}
		else if( Char == '<' || Char == '>' || Char == '|' ){

			if( Status == Str ){

				TokenPile.emplace_back( std::move(Buffer) );
				Buffer.clear();
				Status = Ready;

				}

			char Temp[2] = {Char, '\0'};

			TokenPile.emplace_back( Temp );

			}
		else if( isblank(Char) ){

			if( Status == Str ){

				TokenPile.emplace_back( std::move(Buffer) );
				Buffer.clear();
				Status = Ready;

				}

			}
		else{

			if( Status == Ready )
				Status = Str;

			Buffer += Char;

			}

		}

	if( Status != Ready )
		throw "Invalid command.";

	return TokenPile;

	}



std::vector<command_t> Parse(const std::vector<std::string>& TokenPile){

	std::vector<command_t> CommandQueue;
	command_t Command;
	enum { Ready, Stdin, Stdout } Status = Ready;

	for(const std::string& Token: TokenPile){

		if( Token == "|" ){

			if( Status != Ready )
				throw "Redirect target not specified before \'|\'.";
			else if( Command.Argument.empty() )
				throw "No command appeared before \'|\'";

			CommandQueue.push_back( std::move(Command) );
			Status = Ready;

			}
		else if( Token == "<" ){

			if( Status != Ready )
				throw "\'<\' is not a valid target to redirect.";
			else if( Command.Stdin != nullptr )
				throw "Redirect stdin target already specified.";
			else if( Command.Argument.empty() )
				throw "No command appeared before \'<\'";

			Status = Stdin;

			}
		else if( Token == ">" ){

			if( Status != Ready )
				throw "\'>\' is not a valid target to redirect.";
			else if( Command.Stdout != nullptr )
				throw "Redirect stdout target already specified.";
			else if( Command.Argument.empty() )
				throw "No command appeared before \'>\'";

			Status = Stdout;

			}
		else{

			if( Status == Stdin ){

				Command.Stdin = strdup( Token.c_str() );
				Status = Ready;

				}
			else if( Status == Stdout ){

				Command.Stdout = strdup( Token.c_str() );
				Status = Ready;

				}
			else
				Command.Argument.emplace_back( strdup(Token.c_str()) );

			}

		}

	if( Status != Ready )
		throw "Redirect target not specified.";

	if( Command.Argument.empty() ){

		if( ! CommandQueue.empty() )
			throw "Nothing appeared after \'|\'";

		}
	// Last command.
	else
		CommandQueue.push_back( std::move(Command) );

	return CommandQueue;

	}
