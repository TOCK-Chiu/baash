
CXX      = g++
CXXFLAGS = -Wall -Wextra -O2 -std=c++14 -pedantic -flto
LDFLAGS  = -s
TARGET   = baash
SRCS     = $(wildcard *.cpp)
OBJS     = ${SRCS:%.cpp=%.o}
REV      = $(shell git rev-parse --short HEAD)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -fpie -o $(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -D__REVISION__="\"g$(REV)\"" -fPIE $< -c -o $@

.PHONY: clean

clean:
	$(RM) $(TARGET) *.o
